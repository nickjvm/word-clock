var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');
var path = require('path');
var concat = require('gulp-concat');
var uglifyjs = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var livereload = require('gulp-livereload');
var rename = require('gulp-rename');
var notify = require("gulp-notify");
var autoprefixer = require('gulp-autoprefixer');
var jshint = require('gulp-jshint');

gulp.task('less', function () {

	gulp.src(['./src/less/*.less','./src/less/font-awesome/font-awesome.less'])
		.pipe(less())
		.on("error", notify.onError({
			title:'Oops!',
			message:'LESS Error: <%= error.message %>'
		}))
		.pipe(autoprefixer())
		// Store the 'compiled' CSS in the 'bundle' folder.  Later, we should actually bundle it too.
		.pipe(gulp.dest('./bundle/css'))
		.pipe(minifyCss())
		.pipe(rename(function(path) {
			path.basename += '.min';
		}))
		.pipe(gulp.dest('./dist/css'))
		.pipe(livereload())

	gulp.src(['./src/fonts/*'])
		.pipe(gulp.dest('./bundle/fonts'))
		.pipe(gulp.dest('./dist/fonts'));
});

gulp.task('js',function() {
	gulp.src('./src/js/**/*.js')
		.pipe(gulp.dest('./bundle/js/'))
		.pipe(uglifyjs())
		.on("error", notify.onError({
			title:'Oops!',
			message:'JS Error: <%= error.message %>'
		}))
		.pipe(rename(function(path) {
			path.basename += '.min';
		}))
		.pipe(gulp.dest('./dist/js'));

	gulp.src(['./dist/js/vendor/**/*.js','!./dist/js/vendor/all.min.js'])
		.pipe(concat('all.min.js'))
		.pipe(gulp.dest('./dist/js/vendor'))

	gulp.start('jshint');
});

gulp.task('jshint',function() {
	gulp.src(['./src/js/**/*.js','!./src/js/{vendor,vendor/**/*.js}'])
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(notify(function (file) {
			if (file.jshint.success) {
				// Don't show something if success
				return false;
			}

		  var errors = file.jshint.results.map(function (data) {
			if (data.error) {
				return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
			}
		}).join("\n");
		return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
	}));
});

gulp.task('default',function() {
	gulp.start(['less','js']);

	gulp.watch('./src/less/**/*.less', ['less']);
	gulp.watch('./src/js/**/*.js', ['js']);
});