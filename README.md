# README #

### What is this? ###

My goal for 2015 is a hack per month. January 2015 is a word clock, inspired by [this Make article](http://makezine.com/projects/small-word-clock/)

[View the results here](http://nickvanmeter.com/projects/word-clock)

### Libraries Used ###

* jQuery
* MomentJS