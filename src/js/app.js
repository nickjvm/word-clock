(function() {
	/**
	 * Hack Year 2015 - January: Word Clock. 
	 * Takes the current time digits and converts to English human-readible strings
	 * @author Nick VanMeter / nick@nickvanmeter.com
	 * @version 0.0.1
	 */
	var WordClock = function() {
		var self = this;

		self.Time = moment();

		//The intial interval should only be the number of seconds until the top of the next minute.
		self.Interval = (60 - self.Time.seconds()) * 1000;

		self.Mode = localStorage.getItem("mode") || "light";

		self.init = function() {
			self.CheckTime();
			self.SetMode(self.Mode);

			//Use setTimeout for the first refresh, to get the refresh to begin happening at the top of each minute
			setTimeout(function() {
				self.CheckTime();
				//after the first less-than-60-second interval has run, now we can setup the recurring interval.
				self.SetupInterval();
			},self.Interval);
		};

		/**
		 * Sets the theme for the clock
		 * @param  {string} "dark" or "light"
		 * @return {none}
		 */
		self.SetMode = function(mode) {
			if(mode == "dark") {
				$("body").addClass("dark");
			} else {
				$("body").removeClass("dark");
			}
			localStorage.setItem("mode",mode);
		};

		/**
		 * Creates the 60-second interval between clock refreshes
		 * @param  {none}
		 * @return {none}
		 */
		self.SetupInterval = function() {
			self.Interval = 60000;
			setInterval(self.CheckTime,self.Interval);
		};

		/**
		 * Sets self.Time to the current moment() and begins time parsing process
		 * @param  {none}
		 * @return {none}
		 */	
		 	self.CheckTime = function() {
			self.Time = moment();
			self.BreakUpTime();

			//console.log(moment().format());
		};

		/**
		 * Begins parsing the moment into day / hour / minute pieces
		 * @param  {none}
		 * @return {none}
		 */
		self.BreakUpTime = function() {
			var time = self.Time;
			var newHour = false;
			var newMinute = false;
			var newDay = false;

			var tempMinute = self.RoundMinute(self.Time.minute());

			if(tempMinute != self.Minute) {
				self.Minute = tempMinute;
				newMinute = true;
			}

			var tempHour = self.ConvertHour(self.Time.hour());

			if(self.Minute > 30) {
				tempHour = self.ConvertHour(tempHour + 1);
			}

			if(tempHour != self.Hour) {
				self.Hour = tempHour;
				newHour = true;
			}
			
			if(self.Time.day() != self.Day) {
				newDay = true;
			}

			self.RefreshClock(newHour,newMinute,newDay);

		};

		/**
		 * Calls necessary jQuery selectors to update the clock's HTML classes to display the (new) time.
		 * @param  {boolean} - has the hour changed since the last refresh?
		 * @param  {boolean} - has the minute changed since the last refresh?
		 * @param  {boolean} - has the day changed since the last refresh?
		 * @return {none}
		 */
		self.RefreshClock = function(newHour,newMinute,newDay) {
			var $to = $("[data-word='to']");
			var $past = $("[data-word='past']");
			var $oclock = $("[data-word='oclock']");

			if(newMinute) {
				$(".minute.active").removeClass('active');

				if(self.Minute > 0 && self.Minute < 60) {
					$oclock.removeClass("active");
					if(self.Minute == 25 || self.Minute == 35) {
						$(".minute[data-word='twenty'].minute ,.minute[data-word='five']").addClass("active");
					} else {
						$(".minute[data-word="+ self.Dictionary[self.Minute] + "]").addClass("active");
					}
					if(self.Minute > 30) {
						$to.addClass("active");
						$past.removeClass("active");
					} else {
						$past.addClass("active");
						$to.removeClass("active");
					}
				} else {
					$past.removeClass("active");
					$to.removeClass("active");
					$oclock.addClass("active");
				}
			}

			if(newHour) {
				$(".hour.active").removeClass('active');
				$(".hour[data-word="+self.Dictionary[self.Hour] + "]").addClass("active");
			}
			if(newDay) {
				self.CheckBirthday();
			}
			
		};

		/**
		 * Compares the current day to the stored birthday (if set)
		 * @param  {none}
		 * @return {none}
		 */
		self.CheckBirthday = function() {
			var $birthdayWrapper = $(".birthday");
			
			if(localStorage.getItem("birthday")) {
				self.Birthday = moment(localStorage.getItem("birthday"),"M/DD/YYYY");
				self.BirthMonth = self.Birthday.month() + 1;
				self.BirthDay = self.Birthday.date();

				$(".birthday-month").val(self.BirthMonth);
				$(".birthday-day").val(self.BirthDay);


				if(self.Time.isSame(self.Birthday,"day")) {
					$birthdayWrapper.addClass("active");

					return true;
				} else {
					$birthdayWrapper.removeClass("active");

					return false;
				}
			} else {
				self.Birthday = undefined;
				$birthdayWrapper.removeClass("active");

				return false;
			}
	
		};

		/**
		 * Rounds the current time's minute to the nearest 5
		 * @param  {number} - the current time's minute
		 * @return {number} - rounded to the nearest 5
		 */
		self.RoundMinute = function(int) {
			return 5 * Math.round(int/5);
		};

		/**
		 * Converts the current time's hour in 24-hour time to 12-hour time.
		 * @param  {number} - 1 through 24, the current time's hour
		 * @return {number} - 1 through 12, the curren't time's hour in 12-hour format
		 */
		self.ConvertHour = function(int) {
			return int > 12 ? int - 12 : int;
		};

		/**
		 * Takes new Birthday month and updates full birthday in localStorage
		 * @param  {number} - Birthday month (1-12)
		 * @return {none}
		 */
		self.SetBirthMonth = function(value) {
			self.BirthMonth = value;
			if(self.BirthMonth && self.BirthDay) {
				self.SaveBirthday();
			} else {
				self.RemoveBirthday();
			}
		};

		/**
		 * Takes new Birthday day and updates full birthday in localStorage
		 * @param  {number} - Birthday day (1-31)
		 * @return {none}
		 */
		self.SetBirthDay = function(value) {
			self.BirthDay = value;
			if(self.BirthMonth && self.BirthDay) {
				self.SaveBirthday();
			} else {
				self.RemoveBirthday();
			}
		};

		/**
		 * takes self.BirthDay and self.BirthMonth values, converts to a date and saves into localStorage
		 * as localStorage.birthday
		 * @param  {none}
		 * @return {none}
		 */
		self.SaveBirthday = function() {
			localStorage.birthday = self.BirthMonth + "/" +self.BirthDay + "/" + self.Time.year();

			self.RefreshClock(false,false,true);
		};

		/**
		 * Removes localStorage.birthday from browser resources
		 * @param  {none}
		 * @return {none}
		 */
		self.RemoveBirthday = function() {
			localStorage.removeItem("birthday");
			self.Birthday = undefined;
			self.RefreshClock(false,false,true);
		};


		/**
		 * A dictionary storing the string version of a given integer
		 */
		self.Dictionary = {
			0:"oclock",
			1:"one",
			2:"two",
			3:"three",
			4:"four",
			5:"five",
			6:"six",
			7:"seven",
			8:"eight",
			9:"nine",
			10:"ten",
			11:"eleven",
			12:"twelve", 
			15:"quarter",
			20:"twenty",
			30:"half",
			40:"twenty",
			45:"quarter",
			50:"ten",
			55:"five",
			60:"oclock"
		};

		self.init();

		return self;
	};


	var clock = new WordClock();

	$(document).ready(function() {

		$(".mode-toggle").on("click",function() {
			clock.SetMode($(this).data("mode"));
		});

		$(".js-set-birthday").on('click',function() {
			$(".my-birthday").toggleClass("hidden");
		});

		$(".birthday-month").on("change",function() {
			clock.SetBirthMonth($(this).val());
		});
		$(".birthday-day").on("change",function() {
			clock.SetBirthDay($(this).val());
		});
		
		setTimeout(function() {
			//don't animate the body background color on initial load - this prevents a flash of white to black when in dark mode.
			$("body").addClass("animate");
		},0);
	});

})();
